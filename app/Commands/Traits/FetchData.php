<?php

namespace App\Commands\Traits;

use App\Models\User;

trait FetchData
{
    /**
     * Find user by ID.
     *
     * @param  int $id
     * @return \App\Models\User
     */
    public function findUser($id)
    {
        $user = User::find($id);

        if (! $user) {
            $this->error('User not found');
            return;
        }

        return $user;
    }
}
