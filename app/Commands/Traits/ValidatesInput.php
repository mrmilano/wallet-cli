<?php

namespace App\Commands\Traits;

trait ValidatesInput
{
    /**
     * Validate amount input.
     *
     * @param  int $amount
     * @return bool
     */
    protected function validateAmount($amount)
    {
        if (! is_numeric($amount)) {
            $this->error('Amount must be a numeric value');
            return false;
        } elseif ($amount <= 0) {
            $this->error('Amount must be greater than 0');
            return false;
        }

        return true;
    }
}
