<?php

namespace App\Commands;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Str;
use LaravelZero\Framework\Commands\Command;

class UserCommnad extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'user';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Interactive User commands';

    /**
     * @var array
     */
    protected $availableCommands = [
        'list' => 'Display list of all users',
        'account' => 'Dispaly all user accounts',
        'create' => 'Create new user',
        'setting' => 'Update user settings',
        'topup' => 'Top-up',
        'withdraw' => 'Withdraw',
        'transfer' => 'Transfer',
        'create-account' => 'Create new user account',
        'default-account' => 'Set user default account',
        'freeze-account' => 'Freeze user account',
        'unfreeze-account' => 'Unfreeze user account',
    ];

    /**
     * @var array
     */
    protected $options = [];

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $command = $this->choice('What command are you running?', $this->availableCommands);

        if (! isset($this->availableCommands[$command])) {
            $this->info('The specified command does not exist.');
            return;
        }

        $method = 'user'.Str::studly($command);

        if (method_exists($this, $method)) {
            call_user_func([$this, $method]);
        }

        if (! in_array($command, ['list', 'create'])) {
            $this->askForUserId();
        }

        $this->call('user:'. $command, $this->options);
    }

    /**
     * Ask for user id.
     *
     * @return void
     */
    protected function askForUserId()
    {
        $this->options['user'] = $this->ask('Please enter user ID');
    }

    /**
	 * Define the command's schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule $schedule
	 *
	 * @return void
	 */
	public function schedule(Schedule $schedule): void
	{
		// $schedule->command(static::class)->everyMinute();
	}
}
