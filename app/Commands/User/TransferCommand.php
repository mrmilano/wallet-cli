<?php

namespace App\Commands\User;

use App\Commands\Traits\FetchData;
use App\Commands\Traits\ValidatesInput;
use App\Models\Account;
use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;

class TransferCommand extends Command
{
    use FetchData, ValidatesInput;

    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'user:transfer
                                {user : The ID of the user}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Transfer amounts from one account to another';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $user = $this->findUser($this->argument('user'));
        $choices = $user->getAccountOptions();
        $senderId = $this->menu('Please select your sender account', $choices)->open();
        $sender = $user->accounts->where('id', $senderId)->first();

        if (! $sender) {
            $this->error('Sender account not found');
            return;
        }

        array_forget($choices, $senderId);
        $receiverId = $this->menu('Please select your receiver account', $choices)->open();
        $receiver = $user->accounts->where('id', $receiverId)->first();

        if (! $receiver) {
            $this->error('Receiver account not found');
            return;
        }

        while (! $this->getAmount($sender, $amount)) {
            // Do nothing, just ask for the amount again
        }

        if ($this->confirm("You're going to transfer {$amount} from `{$sender->name}` account to `{$receiver->name}` account. Do you wish to continue?")) {
            // Perform the transfer
            $sender->transfer($amount, $receiver);
            $this->notify('Success', "Transfered {$amount} from `{$sender->name}` account to `{$receiver->name}` account");
            $this->call('user:account', ['user' => $user->id]);
        }
    }

    /**
     * Get amount value.
     *
     * @param  \App\Models\Account $account
     * @param  int &$amount
     * @return bool
     */
    protected function getAmount(Account $account, &$amount)
    {
        $amount = $this->ask('Please enter the amount');

        if ($this->validateAmount($amount)) {
            if ($amount > $account->amount) {
                $this->error('Insufficient funds');
                return false;
            }

            return true;
        }

        return false;
    }

    /**
	 * Define the command's schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule $schedule
	 *
	 * @return void
	 */
	public function schedule(Schedule $schedule): void
	{
		// $schedule->command(static::class)->everyMinute();
	}
}
