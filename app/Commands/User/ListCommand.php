<?php

namespace App\Commands\User;

use App\Models\User;
use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;

class ListCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'user:list';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Display list of all users';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $headers = ['ID', 'Name', 'Daily top-up limit', 'Daily withdrawal limit'];

        $users = User::all(['id', 'name', 'topup_limit', 'withdrawal_limit'])->toArray();

        $this->table($headers, $users);
    }

    /**
	 * Define the command's schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule $schedule
	 *
	 * @return void
	 */
	public function schedule(Schedule $schedule): void
	{
		// $schedule->command(static::class)->everyMinute();
	}
}
