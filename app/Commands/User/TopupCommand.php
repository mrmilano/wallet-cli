<?php

namespace App\Commands\User;

use App\Commands\Traits\FetchData;
use App\Commands\Traits\ValidatesInput;
use App\Models\Account;
use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;

class TopupCommand extends Command
{
    use FetchData, ValidatesInput;

    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'user:topup
                                {user : The ID of the user}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Top-up user account';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $user = $this->findUser($this->argument('user'));
        $option = $this->menu('Please select your account', $user->getAccountOptions())->open();
        $account = $user->accounts->where('id', $option)->first();

        if (! $account) {
            $this->error('User account not found');
            return;
        }

        while (! $this->getAmount($account, $amount)) {
            // Do nothing, just ask for the amount again
        }

        $account->topup($amount);

        $this->notify('Success', "Top-up {$amount} to {$account->name} account for {$user->name}");
        $this->call('user:account', ['user' => $user->id]);
    }

    /**
     * Check if the amount is valid.
     *
     * @param  \App\Models\Account $account
     * @param  int &$amount
     * @return bool
     */
    protected function getAmount(Account $account, &$amount)
    {
        $amount = $this->ask('Please enter the amount');

        if ($this->validateAmount($amount)) {
            if (! $account->canTopup($amount, $error)) {
                $this->error($error);
                return false;
            }
        }

        return true;
    }

    /**
	 * Define the command's schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule $schedule
	 *
	 * @return void
	 */
	public function schedule(Schedule $schedule): void
	{
		// $schedule->command(static::class)->everyMinute();
	}
}
