<?php

namespace App\Commands\User;

use App\Commands\Traits\FetchData;
use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;

class FreezeAccount extends Command
{
    use FetchData;

    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'user:freeze-account
                                {user : The ID of the user}
                                {--account= : The ID of the user account}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Freeze user account';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $user = $this->findUser($this->argument('user'));
        $choices = $user->accounts()
            ->where('freezable', '=', 1)
            ->where('active', '=', 1)
            ->pluck('name', 'id')->toArray();
        $accountId = $this->option('account') ?: $this->menu('Please select your account', $choices)->open();
        $account = $user->accounts()->where('id', $accountId)->first();

        if (! $account) {
            $this->error('User account not found');
            return;
        }

        $account->freeze();
        $this->notify('Success', "Account `{$account->name}` freezed");
        $this->call('user:account', ['user' => $user->id]);
    }

    /**
	 * Define the command's schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule $schedule
	 *
	 * @return void
	 */
	public function schedule(Schedule $schedule): void
	{
		// $schedule->command(static::class)->everyMinute();
	}
}
