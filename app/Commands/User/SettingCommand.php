<?php

namespace App\Commands\User;

use App\Commands\Traits\FetchData;
use App\Commands\Traits\ValidatesInput;
use App\Models\User;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Str;
use LaravelZero\Framework\Commands\Command;

class SettingCommand extends Command
{
    use FetchData, ValidatesInput;

    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'user:setting
                                {user : The ID of the user}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Update user settings';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $user = $this->findUser($this->argument('user'));
        $choices = [
            'update_topup_limit' => 'Change user top-up daily limit',
            'update_withdrawal_limit' => 'Change user withdrawal daily limit',
            'set_default_account' => 'Set default account',
        ];
        $option = $this->menu('Please select your action', $choices)->open();
        $method = Str::studly($option);

        if (method_exists($this, $method)) {
            $this->{$method}($user);
        }
    }

    /**
     * Get amount value.
     *
     * @param  int &$amount
     * @return bool
     */
    protected function getAmount(&$amount)
    {
        $amount = $this->ask('Please enter the amount');

        if ($this->validateAmount($amount)) {
            return true;
        }

        return false;
    }

    /**
     * Update user top-up limit.
     *
     * @param  \App\Models\User $user
     * @return void
     */
    protected function updateTopupLimit(User $user)
    {
        while (! $this->getAmount($amount)) {
            // Do nothing, just ask for the amount again
        }

        $user->topup_limit = $amount;
        $user->save();

        $this->notify('Success', "Top-up daily limit for {$user->name} was set to {$amount}");
    }

    /**
     * Update user withdrawal limit.
     *
     * @param  \App\Models\User $user
     * @return void
     */
    protected function updateWithdrawalLimit(User $user)
    {
        while (! $this->getAmount($amount)) {
            // Do nothing, just ask for the amount again
        }

        $user->withdrawal_limit = $amount;
        $user->save();

        $this->notify('Success', "Withdrawal daily limit for {$user->name} was set to {$amount}");
    }

    /**
     * Set default user account.
     *
     * @param  \App\Models\User $user
     * @return void
     */
    protected function setDefaultAccount(User $user)
    {
        $this->call('user:default-account', ['user' => $user->id]);
    }

    /**
	 * Define the command's schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule $schedule
	 *
	 * @return void
	 */
	public function schedule(Schedule $schedule): void
	{
		// $schedule->command(static::class)->everyMinute();
	}
}
