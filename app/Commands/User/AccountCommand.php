<?php

namespace App\Commands\User;

use App\Commands\Traits\FetchData;
use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;

class AccountCommand extends Command
{
    use FetchData;

    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'user:account
                                {user : The ID of the user}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Dispaly all user accounts';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $user = $this->findUser($this->argument('user'));

        $headers = ['ID', 'Name', 'Amount', 'Default', 'Freeze'];
        $accounts = $user->accounts->map(function ($account) {
            return [
                $account->id,
                $account->name,
                $account->amount,
                $account->is_default ? '✔' : '',
                $account->active ? '' : '✔',
            ];
        })
            ->toArray();

        $this->info("List accounts for user: {$user->name}");
        $this->table($headers, $accounts);
    }

    /**
	 * Define the command's schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule $schedule
	 *
	 * @return void
	 */
	public function schedule(Schedule $schedule): void
	{
		// $schedule->command(static::class)->everyMinute();
	}
}
