<?php

namespace App\Commands\User;

use App\Commands\Traits\FetchData;
use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;

class DefaultAccount extends Command
{
    use FetchData;

    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'user:default-account
                                {user : The ID of the user}
                                {--account= : The ID of the user account}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Set user default account';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $user = $this->findUser($this->argument('user'));
        $choices = $user->accounts()
            ->where('defaultable', '=', 1)
            ->pluck('name', 'id')->toArray();
        $accountId = $this->option('account') ?: $this->menu('Please select your account', $choices)->open();
        $account = $user->accounts()->where('id', $accountId)->first();

        if (! $account) {
            $this->error('User account not found');
            return;
        }

        $account->setAsDefault();
        $this->notify('Success', "Account `{$account->name}` set as default for user `{$user->name}`");
        $this->call('user:account', ['user' => $user->id]);
    }

    /**
	 * Define the command's schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule $schedule
	 *
	 * @return void
	 */
	public function schedule(Schedule $schedule): void
	{
		// $schedule->command(static::class)->everyMinute();
	}
}
