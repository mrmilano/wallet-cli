<?php

namespace App\Commands\User;

use App\Models\User;
use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;

class CreateCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'user:create
                                {name? : Name of the user}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Create a new user';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $name = $this->argument('name') ?: $this->ask('Please enter user name');

        $user = User::create([
            'name' => $name,
            'topup_limit' => User::TOPUP_LIMIT,
            'withdrawal_limit' => User::WITHDRAWAL_LIMIT,
        ]);

        $this->notify('Success', "User `{$user->name}` created");
        $this->call('user:account', ['user' => $user->id]);
    }

    /**
	 * Define the command's schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule $schedule
	 *
	 * @return void
	 */
	public function schedule(Schedule $schedule): void
	{
		// $schedule->command(static::class)->everyMinute();
	}
}
