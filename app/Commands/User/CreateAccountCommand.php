<?php

namespace App\Commands\User;

use App\Commands\Traits\FetchData;
use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;

class CreateAccountCommand extends Command
{
    use FetchData;

    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'user:create-account
                                {user : The ID of the user}
                                {--name= : The currency of the account}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Create new user account';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $user = $this->findUser($this->argument('user'));
        $name = $this->option('name') ?: $this->ask('Please enter the account currency name');

        if ($name) {
            $account = $user->accounts()->create([
                'name' => $name,
            ]);

            $this->notify('Success', "Account `{$name}` created for user `{$user->name}`");
            $this->call('user:account', ['user' => $user->id]);
        }
    }

    /**
	 * Define the command's schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule $schedule
	 *
	 * @return void
	 */
	public function schedule(Schedule $schedule): void
	{
		// $schedule->command(static::class)->everyMinute();
	}
}
