<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class User extends Model
{
    /**
     * @var int
     */
    const TOPUP_LIMIT = 100;
    const WITHDRAWAL_LIMIT = 100;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'topup_limit',
        'withdrawal_limit',
    ];

    /**
     * The "boot" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        static::created(function ($user) {
            $user->createDefaultAccounts();
        });
    }

    /**
     * Get all user accounts.
     *
     * @return \Illuminate\Database\Eloquent\Relations\Relation
     */
    public function accounts()
    {
        return $this->hasMany(Account::class);
    }

    /**
     * Get all user transactions.
     *
     * @return \Illuminate\Database\Eloquent\Relations\Relation
     */
    public function transactions()
    {
        return $this->hasManyThrough(Transaction::class, Account::class);
    }

    /**
     * Create user default e-wallet accounts.
     *
     * @return void
     */
    public function createDefaultAccounts()
    {
        $virtual = new Account([
            'name' => 'Virtual',
            'amount'  => 0,
        ]);
        $virtual->defaultable = false;
        $virtual->freezable = false;
        $virtual->withdrawable = false;

        $usd = new Account([
            'name' => 'USD',
            'amount'  => 0,
        ]);
        $usd->is_default = true;

        $this->accounts()->saveMany([$virtual, $usd]);
    }

    /**
     * Get user top-up limit.
     *
     * @param  mixed $day
     * @return int
     */
    public function getTopupLimit($day = null)
    {
        $date = Carbon::parse($day);

        // Get user transactions on a specific day.
        $amount = $this->transactions()
            ->where('type', '=', 'top-up')
            ->whereDate('transactions.created_at', $date->toDateString())
            ->sum('value');

        $value = $this->topup_limit - $amount;

        return $value > 0 ? $value : 0;
    }

    /**
     * Get user withdrawal limit.
     *
     * @param  mixed $day
     * @return int
     */
    public function getWithdrawalLimit($day = null)
    {
        $date = Carbon::parse($day);

        // Get user transactions on a specific day.
        $amount = $this->transactions()
            ->where('type', '=', 'withdrawal')
            ->whereDate('transactions.created_at', $date->toDateString())
            ->sum('value');
        $value = $this->withdrawal_limit - $amount;

        return $value > 0 ? $value : 0;
    }

    /**
     * Get account options for selection.
     *
     * @param  \App\Models\User $user
     * @return array
     */
    public function getAccountOptions()
    {
        return $this->accounts->keyBy('id')
            ->map(function ($account) {
                return sprintf('%s - (%d)', $account->name, $account->amount);
            })->toArray();
    }
}
