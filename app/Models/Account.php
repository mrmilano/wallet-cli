<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'amount',
    ];

    /**
     * Get the user owned this account.
     *
     * @return \Illuminate\Database\Eloquent\Relations\Relation
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get all user account transactions.
     *
     * @return \Illuminate\Database\Eloquent\Relations\Relation
     */
    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    /**
     * Check if current account can be top-uped.
     *
     * @param  int $amount
     * @param  string $error
     * @return bool
     */
    public function canTopup($amount, &$error = '')
    {
        $limit = $this->user->getTopupLimit();

        if ($amount > $limit) {
            $error = $limit
                ? 'Amount must be lower than your top-up limit'
                : 'You\'ve reached your daily top-up limit';
            return false;
        }

        return true;
    }

    /**
     * Top-up.
     *
     * @param  int $amount
     * @return void
     */
    public function topup($amount)
    {
        $oldVal = $this->amount;

        $this->increment('amount', intval($amount));
        $this->transactions()->create([
            'type' => 'top-up',
            'value' => $amount,
            'old_value' => $oldVal,
            'new_value' => $this->amount,
        ]);
    }

    /**
     * Check if current account can be withdrawed.
     *
     * @param  int $amount
     * @param  string $error
     * @return bool
     */
    public function canWithdraw($amount, &$error = '')
    {
        if (! $this->withdrawable) {
            $error = 'This account can\'t be withdrawn';
            return false;
        }

        $limit = $this->user->getWithdrawalLimit();

        if ($amount > $this->user->getWithdrawalLimit()) {
            $error = $limit
                ? 'Amount must be lower than your withdrawal limit'
                : 'You\'ve reached your daily withdrawal limit';
            return false;
        }

        return true;
    }

    /**
     * Withdraw.
     *
     * @param  int $amount
     * @return void
     */
    public function withdraw($amount)
    {
        $oldVal = $this->amount;

        if ($amount > $oldVal) {
            return;
        }

        $this->decrement('amount', intval($amount));
        $this->transactions()->create([
            'type' => 'withdrawal',
            'value' => $amount,
            'old_value' => $oldVal,
            'new_value' => $this->amount,
        ]);
    }

    /**
     * Transfer between accounts.
     *
     * @param  int $amount
     * @param  \App\Models\Account $account
     * @return void
     */
    public function transfer($amount, Account $account)
    {
        if ($amount > $this->amount) {
            return;
        }

        $this->decrement('amount', intval($amount));
        $account->increment('amount', intval($amount));
    }

    /**
     * Set current account as default.
     *
     * @return void
     */
    public function setAsDefault()
    {
        if (! $this->defaultable) {
            return;
        }

        static::where('user_id', '=', $this->user_id)
            ->where('id', '<>', $this->id)
            ->update([
                'is_default' => false
            ]);

        $this->is_default = true;
        $this->save();
    }

    /**
     * Freeze current account.
     *
     * @return void
     */
    public function freeze()
    {
        $this->setActive(false);
    }

    /**
     * Unfreeze current account.
     *
     * @return void
     */
    public function unfreeze()
    {
        $this->setActive(true);
    }

    /**
     * Change the active attribute.
     *
     * @param  bool $active
     * @return void
     */
    public function setActive($active = true)
    {
        if (! $this->freezable) {
            return;
        }

        $this->active = $active;
        $this->save();
    }
}
