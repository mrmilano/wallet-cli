
# Wallet CLI


## Overview

- List all users
- Create new user
- Update user settings
- List and create user accounts
- Top-up, withdraw, transfer amount

## Requirements

- PHP 7.1.3+ or newer
- [Composer\](https://getcomposer.org)
- A supported database: SQLite

## Installation

- Download the source code manually or checkout with `git`
- Install all dependencies with composer
    ```
    composer install
    ```
- Create SQlite database
    ```
    touch database/database.sqlite
    ```
- Run the migration
    ```
    php wallet migrate
    ```

## Usage

To run your application, just execute in your application root folder:

### Interactive command

```
php wallet user
```
> Please follow the onscreen prompts to perform an user action

### Available commands

- `php wallet user:list`: Display list of all users.
- `php wallet user:create`: Create new user.
- `php wallet user:account {UserID}`: Dispaly all user accounts.
- `php wallet user:setting {UserID}`: Update user settings.
- `php wallet user:topup {UserID}`: Top-up.
- `php wallet user:withdraw {UserID}`: Withdraw.
- `php wallet user:transfer {UserID}`: Transfer.
- `php wallet user:create-account {UserID}`: Create new user account.
- `php wallet user:default-account {UserID}`: Set user default account.
- `php wallet user:freeze-account {UserID}`: Freeze user account.
- `php wallet user:unfreeze-account {UserID}`: Unfreeze user account.

To see all available commands please use the command
```
php wallet
```
